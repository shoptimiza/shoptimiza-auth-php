<?php
declare(strict_types=1);

require "ShoptimizaAuth.php";


use PHPUnit\Framework\TestCase;
use Shoptimiza\Auth as Auth;

final class ShoptimizaAuthTest extends TestCase
{
    const APIKEY = "134654";
    const NOW = "1518619891";
    const SECRET = "secret";

    protected static function getMethod($name) {
        $class = new ReflectionClass('Shoptimiza\Auth');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    public function testSignature(){
        $verb = "GET";
        $url ="shoptimiza.com/hola";

        $expected = "SOvueGN73WAkfa6DguAZ7EEHKaMczZUsjdtKWfsK/r0=";
        $actual= Auth::signature(self::APIKEY, self::NOW, $verb, $url, self::SECRET);
        $this->assertEquals($expected, $actual);
    }


    public function testPrivateApiKeyMismatch(){
        $a = '123';
        $b = 'abc';
        $fn = self::getMethod('apiKeyMismatch');
        $result = $fn->invokeArgs(null, array($a,$b));
        $this-> assertEquals(true, $result);

    }
    public function testCheckSignatureFailsWhenNoArgumentsProvided() {
        $expectedMessage = 'function signature is $signature, $options';
        try{
            Auth::checkSignature(null, null);
        }catch(Exception $e){
            $this->assertEquals($expectedMessage, $e->getMessage());
            return;
        }
        $this->assertFalse(true, 'expected exception did not happen');
    }

    public function testCheckSignatureFailsWhenSignatureIsNotAString() {
        $expectedMessage = '$signature should be a string';
        try{
            Auth::checkSignature(1, array());
        }catch(Exception $e){
            $this->assertEquals($expectedMessage, $e->getMessage());
            return;
        }
        $this->assertFalse(true, 'expected exception did not happen');
    }

    public function testCheckSignatureFailsWhenOptionsIsNotAnArray() {
        $expectedMessage = '$options should be an array';
        try{
            Auth::checkSignature('', 1);
        }catch(Exception $e){
            $this->assertEquals($expectedMessage, $e->getMessage());
            return;
        }
        $this->assertFalse(true, 'expected exception did not happen');
    }

    public function testCheckSignature() {

        $options = array(
            'now' => self::NOW,
            'apiKey' => self::APIKEY,
            'secret' => self::SECRET,
            'verb' => 'GET',
            'timeout' => 3,
            'url' => 'shoptimiza.com/hola',
        );
        $incomingSignature = self::APIKEY.'.'.self::NOW.'.SOvueGN73WAkfa6DguAZ7EEHKaMczZUsjdtKWfsK/r0=';

        $result = Auth::checkSignature($incomingSignature, $options);
        $this->assertEquals(false, $result);
    }
    public function testHeaderConstant() {

        $expected = 'X-Shoptimiza-Auth';

        $this->assertEquals($expected, Auth::HEADER);
    }
}

