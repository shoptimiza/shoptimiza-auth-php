<?php

namespace Shoptimiza;
use \InvalidArgumentException as InvalidArgumentException;

class Auth
{
    const HEADER='X-Shoptimiza-Auth';
    public static function signature($apiKey, $time, $verb, $url, $secret) {
        $input = array($apiKey, $time, strtoupper($verb),$url);
        $str = $apiKey.'.'.$time.'.'.$verb.'.'.$url;
        return base64_encode(hash_hmac('sha256', $str, $secret, true));
    }
    private static function wrongNumberOfParts($segments){
        return count($segments) !== 3;
    }
    private static function timedout($now, $signatureTime, $timeout){
        $maxTimeDelta = abs($signatureTime - $now);
        return ($maxTimeDelta > $timeout);
    }
    private static function apiKeyMismatch($a, $b) {
        return $a !== $b;
    }
    public static function checkSignature($signature, $options) {

        if (!isset($signature, $options)) {
            throw new InvalidArgumentException('function signature is $signature, $options');
        }
        if (!is_string($signature)) {
            throw new InvalidArgumentException('$signature should be a string');
        }
        if (!is_array($options)) {
            throw new InvalidArgumentException('$options should be an array');
        }

        $now = $options['now'];
        $apiKey = $options['apiKey'];
        $secret = $options['secret'];
        $verb = $options['verb'];
        $url = $options['url'];
        $maxTimeDelta = $options['timeout'];

        $segments = explode('.', $signature);

        if (Auth::wrongNumberOfParts($segments)) {
            return array('reason' => 'invalid signature');
        }

        $signatureApiKey = $segments[0];
        $signatureTime = $segments[1];
        $signatureHash = $segments[2];

        if (Auth::timedout($now, $signatureTime, $maxTimeDelta)) {
            $result['reason'] = 'timeout';
            $result['time'] = $now;
            return array('reason' => 'timeout', 'time' => $now);
        }

        if (Auth::apiKeyMismatch($apiKey, $signatureApiKey)) {
            return array('reason' => 'invalid apiKey');
        }

        $expectedHash = Auth::signature($apiKey, $signatureTime, $verb, $url, $secret);
        if ($expectedHash !== $signatureHash) {
            return array('reason' => 'invalid signature');
        }

        return;
    }
}

